# Hammer Constructora


## Índice

- [1. Installation](https://gitlab.com/daquiroz/hammer-constructora#installation)
- [2. Add projects](https://gitlab.com/daquiroz/hammer-constructora#projects)
- [3. Add images in slideshow](https://gitlab.com/daquiroz/hammer-constructora#slides3)

## Installation

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git clone git@gitlab.com:daquiroz/hammer-constructora.git
```
- ` cd hammer-constructora` to enter the folder site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=
```

### Projects

Los proyectos están ubicados en la carpeta `hammer-constructora > _projects `.

Para agregar un proyecto debe crearse un archivo `nombre-proyecto.md` en la misma carpeta, con la siguiente estructura:

```
---
title: CASA POO - MUÑOZ
code: cpoomunoz
summary: Vivienda Familiar diseñada por Arq. Mario Garbarini e hijo, distribuida en dos niveles más sector de caldera y bodega. La superficie construida es de 232 m2.
type: Vivienda Familiar
years: "2017"
location: Temuco
area: 232 m2
date: 2019-10-21 12:00:00 -0300
main_image: home1_color.png
images:
  - home1_bw.png
  - home1_color.png
  - home1_color.png
---

Mezcla materiales tradicionales como muros de albañilería y hormigón armado que fueron revestidos con “Piedra Pucón” en algunos elementos y otros pintados, además incorpora maderas nativas como Pino Oregón en varios elementos estructurales como así en todos los marcos de puertas y ventanas; El segundo nivel se estructuró en perfiles metálicos tipo Metalcon. Toda la vivienda fue completamente aislada con poliestireno expandido y lana de vidrio cumpliendo las exigencias de zonificación térmica. Cuenta con calefacción mediante radiadores alimentada por una caldera a Pellet. Porcelanato en pisos de nivel 2 más todos los baños en diversos diseños (marcas CHC y Atika). Segundo piso con pavimento tipo piso flotante (Atika).

La casa en el primer nivel cuenta con un dormitorio principal en suite, sala de estar y baño de visita, hall de acceso independiente, living, comedor y cocina, además de un sector que alberga sala de caldera, bodega y destaca un circuito de alimentación eléctrica independiente para alimentarse con generador en caso de corte de energía.  El segundo nivel tiene dos dormitorios un baño, sala de estar y oficina.   
```

Algunas consideraciones importantes:
* Las imagenes deben agregarse en la carpeta `hammer-constructora > assets >  img > buildings`.
* La imagen `main_image` es la que se muestra en el home, mientras que las imagenes listadas en `images` son las que se muestran en la vista particular del proyecto.

### Slideshow

En construcción
