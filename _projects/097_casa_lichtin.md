---
layout: project
title: CASA LICHTIN - PREUSS
code: clichtin
summary: Vivienda familiar diseñada por Arq. Carlos Gómez y Jaime Gatica, distribuida en un solo nivel y que incorpora como material principal la madera potenciada por el trabajo en conjunto la empresa TIMBER.
type: Vivienda Familiar
years: "2019-2020"
location: Lautaro
area: 324 m2 + 180 m2 terrazas.
main_image: Lichtin/CasaLichtin6.jpg
date: 2019-10-21 12:00:00 -0300
images:
  - Lichtin/CasaLichtin1.jpg
  - Lichtin/CasaLichtin2.jpg
  - Lichtin/CasaLichtin3.jpg
  - Lichtin/CasaLichtin4.jpg
  - Lichtin/CasaLichtin5.jpg
  - Lichtin/CasaLichtin6.jpg
  - Lichtin/CasaLichtin7.jpg
  - Lichtin/CasaLichtin8.jpg
  - Lichtin/CasaLichtin9.jpg
---

La obra fue proyectada en conjunto con la empresa Timber que provee maderas de alto nivel y que provienen de bosques gestionados con criterios de sostenibilidad. Este trabajo en conjunto permite acortar los tiempos de construcción ya que las piezas entregadas por Timber vienen pre cortadas y se montan siguiendo una secuencia de montaje.

Cuenta con amplios espacios entre los que se destacan un dormitorio en suite de 60 m2 con baño y walk in closet, núcleo central común que incorpora un living con chimenea en obra , comedor y cocina completamente conectados que genera un ambiente de relación común entre los ocupantes y un sector de dormitorios amplios e iluminados. La vivienda tiene una doble altura en toda su superficie que entrega una agradable sensación de amplitud. Estacionamiento techado para dos vehículos además de una terraza y quincho techado de 120 m2.

Destaca una importante aislación térmica en toda la envolvente y sistemas de ventilación activa/pasiva que optimizan el confort interno de la vivienda.    
